package main

import (
	"fmt"
	"sync"
)

func runThingsConcurrently(chIn <-chan int, chOut chan<- string, wg *sync.WaitGroup) {
	defer wg.Done()
	for val := range chIn {
		go func(val int) {
			resultUnicode := doBusinessLogic(val)
			fmt.Printf("unicode of this number from chIn channel: %d → %s\n", val, resultUnicode)
			chOut <- resultUnicode
		}(val)
	}
}

func doBusinessLogic(val int) string {
	return fmt.Sprintf("%c", val)
}

func main() {
	var wg sync.WaitGroup

	chIn :=make(chan int)
	chOut :=make(chan string)

	wg.Add(10)
	for i := 7; i < 10; i++ {
		go runThingsConcurrently(chIn, chOut, &wg)
		chIn <- i
	}
}